package ru.terrakok.gitlabclient.presentation.global

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

/**
 * Created by Konstantin Tskhovrebov (aka @terrakok) on 24.07.17.
 */
class GlobalMenuController {
    private val flow = MutableSharedFlow<Boolean>()

    val state: Flow<Boolean> = flow
    fun open() = flow.tryEmit(true)
    fun close() = flow.tryEmit(false)
}
